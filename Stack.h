#pragma once
#include "Element.h"

template <typename T>
class Stack
{
private:
	Element<T>* Head;
public:
	Stack(): Head(nullptr)
	{
	}

	~Stack()
	{
		Element<T>* Elem = Head;
		while (Elem)
		{
			Head = Elem->GetNextElement();
			delete Elem;
			Elem = Head;
		}
	}

	void Push(T Value)
	{
		Element<T>* Elem = new Element<T>(Value, Head);
		Head = Elem;
	}

	void Pop(T& Value)
	{
		Element<T>* Elem = Head;

		if (Elem)
		{
			Value = Elem->GetValue();
			Head = Elem->GetNextElement();
			delete Elem;
		}
	}
};

