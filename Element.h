#pragma once

template <typename T>
class Element
{
private:
	T Value;
	Element<T>* NextElement;
public:
	Element(T _Value, Element<T>* _NextElement) : Value(_Value), NextElement(_NextElement)
	{
	}

	T GetValue()
	{
		return Value;
	}

	Element<T>* GetNextElement()
	{
		return NextElement;
	}
};

