﻿#include <iostream>
#include "Stack.h"

int main()
{
    Stack<int> StackExample;
    int elem;

    StackExample.Push(10);
    StackExample.Push(20);
    StackExample.Push(30);

    StackExample.Pop(elem);
    std::cout << elem << "\n";
    StackExample.Pop(elem);
    std::cout << elem << "\n";
    StackExample.Pop(elem);
    std::cout << elem << "\n";
}